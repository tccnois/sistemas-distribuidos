import java.rmi.*;
import java.rmi.RemoteException;
import java.rmi.Remote;
import java.io.*;

public interface AcionistaInt extends Remote {
	public void notificarEventos(String not ) throws RemoteException;	
	public void abreTransacao(int tid) throws RemoteException, IOException;
	public boolean preparar(int tid, String code, int q ,double valor, boolean type) throws RemoteException, IOException;	
    public void efetivar(int tid) throws RemoteException, IOException, InterruptedException;
    public void abortar(int tid) throws RemoteException, InterruptedException;
}