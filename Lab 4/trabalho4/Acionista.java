import java.util.*;
import java.text.*;
import java.rmi.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.Remote;
import java.rmi.server.UnicastRemoteObject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

//  data, hora, Id da transação, estado da transação, etc. 

public class Acionista extends UnicastRemoteObject implements AcionistaInt{
	
	static Double saldo ;   // representa o tanto de dinheiro que o acionista tem
    static AcionistaInt ac;  // objeto do acionista
    static HomebrokerInt s; // objeto do homebroker
    static String c;
    static Map<String,Acao> ativos = new HashMap<String,Acao>(); // representa a lsita de açoes que o acionista tem
	static Map<Integer,String> map_tid_estado = new HashMap<Integer,String>(); // representa a lsita de açoes que o acionista tem
	static Participante	gerenciador_de_transacao;
		

	public Acionista() throws RemoteException{
    }
    static public void setSaldo(Double s){
    	saldo = s;
    }
    public void abreTransacao(int tid) throws RemoteException, IOException{
    	gerenciador_de_transacao.abreTransacao(tid);
    }
    public boolean preparar (int tid ,String code, int q ,double valor, boolean type) throws RemoteException, IOException{
      	return gerenciador_de_transacao.preparar(tid,code,q,valor,type);            
    }
    public void efetivar(int tid) throws RemoteException, IOException, InterruptedException{
    	gerenciador_de_transacao.efetivar(tid);
    }
    public void abortar(int tid) throws RemoteException, InterruptedException{
    	gerenciador_de_transacao.abortar(tid);
    }
    static public void salvar (String path, String name, String data ) throws IOException {
    	FileWriter writer = new FileWriter(path +name,true);
    	PrintWriter gravar = new PrintWriter(writer);
    	gravar.printf(data);
    	writer.close();
    }


    static public void init(String path) throws IOException, InterruptedException{
		

		int MAX = 525287; // variavel usada para dar um valor aleatorio de saldo pro cliente para testes
		Random r = new Random(); 
    	File file = new File (path);
    	
    	if (!file.exists()) file.mkdirs();
    	file = new File (path+"saldo");    	
    	if (!file.exists()){
	    	saldo = (double)((r.nextInt()%1000000)&MAX);  
    		salvar (path,"saldo",String.format("%.02f",saldo));
    	}
    	else{
    		Scanner reader = new Scanner(file);
    		saldo = reader.nextDouble(); // ue?  Double.parseDouble(reader.nextLine());
    	}

    	file = new File (path+"ativos");    

    	if (!file.exists()){
			String code;// usada para decidir quais acoes aleatorias o cliente terá
	    	for (int i = 0; i < 20; i++){
	    		if ((r.nextInt()%2) == 1){
	    			code = "A0"+String.format("%02d",i);
	    			ativos.put(code,new Acao(code,((r.nextInt()&MAX)%100)+1,(double)(((r.nextInt()&MAX)%100+1))));
	    			salvar(path,"ativos",ativos.get(code).printF());
	    			try{
	 	   				if (s.inserirInteresse(c,code)){
	 	   					System.out.println("interesse Inserido");
	 	   				}
	    			}
	    			catch (Exception e) {
			           	System.err.println("Client exception: " + e.toString());
			            e.printStackTrace();
			        }
	    		}
	    	}
    	}
		else{
    		Scanner reader = new Scanner(file);
    		while (reader.hasNextLine()){
    			String d = reader.nextLine();	
    			String[] data = d.split(";");
    			ativos.put(data[0],new Acao(data[0],Integer.parseInt(data[1]),Double.parseDouble(data[2].replace(',','.'))));
    			try{
 	   				if (s.inserirInteresse(c,data[0])){
 	   					System.out.println("interesse Inserido");
 	   				}
    			}
    			catch (Exception e) {
		           	System.err.println("Client exception: " + e.toString());
		            e.printStackTrace();
		        }
    		}
    	}
		gerenciador_de_transacao = new Participante(ativos, saldo, map_tid_estado, c, s);

    }

	public void notificarEventos (String not)throws RemoteException{
        System.out.println("--------------------------------------------------------");
        System.out.println("[ALERTA PROGRAMADO]");
        System.out.println(not);
    	System.out.println("--------------------------------------------------------");
    }
    public static boolean valid(int q, double valor){
    	return saldo >= ((double)q)*valor;
    }    
    public static boolean valid(int q, String codigo){
    	if (ativos.containsKey(codigo))
    		return ativos.get(codigo).quantidade >= q;
    	return false;
    }
    public static void mensagem(String m){
    	System.out.println("Mensagem------------------------------------------------");
    	System.out.println(m);
        System.out.println("--------------------------------------------------------");
    } 

	public static void main(String[] args)throws RemoteException, IOException{
        try {
            Registry registry = LocateRegistry.getRegistry(10099);
            s = (HomebrokerInt) registry.lookup("MarRicoHomebroker");
        	ac = new Acionista();	
            Scanner inp = new Scanner(System.in);
            // ---------------
            System.out.print("username: ");
            c = "."+ inp.nextLine()+"/"; 
            s.registrar(ac,c);
            init(c);
        	new Thread(gerenciador_de_transacao).start();
            // ---------------
            String codigo,datestr;
            Date d;
            SimpleDateFormat form;
            int q;
            double v,l;
           	int op = 8;
            while(op != 0){
				System.out.println("0 - Para sair");
				System.out.println("1 - Obter cotações");
				System.out.println("2 - Criar ordem de compra");
				System.out.println("3 - Criar ordem de venda");
				System.out.println("4 - registrar interesse");
				System.out.println("5 - remover interesse");
				System.out.println("6 - registrar notificacao");
				System.out.println("7 - Ver minha carteira");
				op = inp.nextInt();
                switch (op){
                	case 1:
                		System.out.println(s.obterCotacoes(c));
                		break;
                	case 2:
                		System.out.print("Código da acao: ");inp.nextLine();
                    	codigo = inp.nextLine();
                    	System.out.print("Quantidade: ");
    					q = inp.nextInt();
    					System.out.print("Preço (use vírgula): ");
    					v = inp.nextDouble();
    					System.out.print("Data de validade da ordem (dd/mm/yyyy hh:mm): ");inp.nextLine();
    	                datestr = inp.nextLine();
    	                form = new SimpleDateFormat("dd/MM/yyyy hh:mm");
    		            d = form.parse(datestr);
    		            if (valid(q,v)){
	    		            if (s.criarOrdem(codigo,q,v,d,c,false))
	       					    mensagem("Ordem de compra enviada com sucesso");
    		            	else
    		            		mensagem("Falha na operação, tente novamente");
    		            }
    		            else
    		            	mensagem("Você não tem saldo suficiente");
    					break;
    				case 3:
                		System.out.print("Código da acao: ");inp.nextLine();
                    	codigo = inp.nextLine();
                    	System.out.print("Quantidade: ");
    					q = inp.nextInt();
    					System.out.print("Preço (use vírgula): ");
    					v = inp.nextDouble();
    					System.out.print("Data de validade da ordem (dd/mm/yyyy hh:mm): ");inp.nextLine();
    	                datestr = inp.nextLine();
    	                form = new SimpleDateFormat("dd/MM/yyyy hh:mm");
    		            d = form.parse(datestr);
    		            if (valid(q,codigo)){
	    		            if (s.criarOrdem(codigo,q,v,d,c,true))
	                            mensagem("Ordem de venda enviada com sucesso");
    		            	else
    		            		mensagem("Falha na operação, tente novamente");
    		            }
    		            else
    		            	mensagem("Você não tem a quantidade de ações necesarias para efetuar a venda");

    					break;
    				case 4:
                		System.out.print("Código da acao: ");inp.nextLine();
                    	codigo = inp.nextLine();
    					if (s.inserirInteresse(c,codigo))
    						mensagem("Interesse inserido com sucesso");
    					else 
    						mensagem("Falha na operação, tenta novamente");
    					break;
    				case 5:
     					System.out.print("Código da acao: ");inp.nextLine();
                    	codigo = inp.nextLine();
    					if (s.removerInteresse(c,codigo))
    						mensagem("Interese Removido");
    					else
    						mensagem("Falha na operação, tenta novamente");
    					break;
    				case 6:
                		System.out.print("Código da acao: ");inp.nextLine();
                    	codigo = inp.nextLine();
                    	System.out.print("Limite de ganho(+) ou perda(-): ");
                    	l = inp.nextDouble();
                		s.registrarNotificacao(c, codigo, l);
                		break;
                	case 7:
                		System.out.println("Carteira------------------------------------------------");
                		System.out.println("Saldo:"+Double.toString(saldo));
                		System.out.println("Ativos:");
                		for(String acao : ativos.keySet())
         					ativos.get(acao).print();
                        System.out.println("--------------------------------------------------------");
         				break;
         			default:
         				System.out.println("Entrada INCORRETA!");
                		break;
                }
            }
                
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
	}
}
// Classe que representa uma ação
class Acao {
    String codigo;
    int quantidade;
    double pm; // preço medio de cada ação comprada, valor / quantidade;
    public Acao(String _s, int _q, double _pm){
    	this.codigo = _s;
    	this.quantidade = _q;
    	this.pm = _pm;
    }
 	public void print(){
 		System.out.println("codigo: "+codigo+"| quantidade: "+Integer.toString(quantidade)+"| preco medio: "+ String.format("%.02f",pm));
 	}
 	public String printF(){
 		return (codigo +";"+Integer.toString(quantidade)+";"+ String.format("%.02f",pm) + "\n");
 	}
 	public String printE(){
 		return (codigo +";"+Integer.toString(quantidade)+";"+ String.format("%.02f",pm));
 	}
}

class Task {
	public int tid,task;
	public Task(int t, int w){
		this.tid = t;
		this.task = w;
	}

}



class Participante extends UnicastRemoteObject implements Runnable {
 	
	protected BlockingQueue tasks = new LinkedBlockingQueue<>();
	protected static Queue recurso = new LinkedList<>();

	protected static boolean b_recurso = false;
    protected Map<String,Acao> ativos = null;
    protected Map<Integer,String> map_tid_estado = null;
    protected Double saldo = null;
    protected String c = null;
    protected HomebrokerInt s = null;
    public Participante(Map ativos, Double saldo, Map map_tid_estado,String c,HomebrokerInt s)throws RemoteException,IOException,InterruptedException {
        lock(b_recurso,recurso,-1);
        this.ativos = ativos;
        this.saldo = saldo;
		this.map_tid_estado=map_tid_estado;
		this.c = c;
		this.s = s;
		File f = new File(c+"transacoes");
		if (f.exists()){
			Scanner reader = new Scanner(f);
		   	while (reader.hasNextLine()){
				String d = reader.nextLine();	
				String[] data = d.split(";");
				map_tid_estado.put(Integer.parseInt(data[0]),data[1]);
		    }	
		    for (Integer i : this.map_tid_estado.keySet()){
				if (!map_tid_estado.get(i).equals("EFETIVADA") && !map_tid_estado.get(i).equals("ABORTADA")){
					String est = s.obterEstadoTransacao(i);
					if (est.equals("EFETIVADA"))
						efetivar(i);
					else if (est.equals("ABORTADA"))
						abortar(i);
				}
		    }
		}
		unlock(b_recurso);
    }
    static public void salvar (String path, String name, String data, boolean s ) throws IOException {
    	FileWriter writer = new FileWriter(path +name,s);
    	PrintWriter gravar = new PrintWriter(writer);
    	gravar.printf(data);
    	writer.close();
    }

    static public void lock (Boolean item , Queue q, Object a){
		if (item == false)
			item = true;
		else{
			q.add(a);
			while ( item == true || a != q.peek());
			lock (item,q,a);
			q.remove();
		}
	}
	// unlock algum recurso
	static public void unlock (Boolean item){
		item = false;
	}

    void abreTransacao (int tid) throws IOException{
    	lock(b_recurso,recurso,tid);
    	map_tid_estado.put(tid,"ABERTO");
    	salvar(c,"transacoes",Integer.toString(tid)+";ABERTO\n",true);
    	unlock(b_recurso);
    }
    boolean preparar(int tid, String code, int q ,double valor, boolean type)throws IOException{
    	lock(b_recurso,recurso,tid);
    	double novo_saldo = saldo + valor*(type?1:-1)*q;
    	Acao nova_acao = (!ativos.containsKey(code)? (new Acao(code,0,0)) : (Acao)(ativos.get(code)));
        if (!type)
        	nova_acao.pm = (nova_acao.pm*nova_acao.quantidade + valor*q)/(nova_acao.quantidade+q);
        nova_acao.quantidade += q*(type?-1:1);
        salvar(c,"temporario"+Integer.toString(tid),String.format("%.02f",novo_saldo) +";"+nova_acao.printE()+";"+Boolean.toString(type)+";"+Integer.toString(q)+";"+String.format("%.02f",valor)+"\n",true);
    	unlock(b_recurso);
    	File arq = new File(c + "temporario" + Integer.toString(tid));
    	if (arq.exists())
  	  		return true;
  	  	else 
  	  		return false;
  	  	
    }
    void efetivar(int tid) throws InterruptedException{
    	tasks.put(new Task(tid,0));
    }

    void abortar(int tid) throws InterruptedException{
    	tasks.put(new Task(tid,1));
    }

    void e_l(int tid) throws IOException{
    	lock(b_recurso,recurso,tid);
    	e(tid);
    	salvar(c,"transacoes",Integer.toString(tid)+";EFETIVADA\n",true);
		unlock(b_recurso);
    }

    void e(int tid) throws IOException{
    	File file = new File(c + "temporario" + Integer.toString(tid));
    	Scanner reader = new Scanner(file);
		String d = reader.nextLine();	
		String[] data = d.split(";");
		Acao acao = new Acao(data[1],Integer.parseInt(data[2]),Double.parseDouble(data[3].replace(',','.')));
		ativos.put(data[1],acao);
		try{
			s.inserirInteresse(c,data[1]);	
		}
		catch (Exception e) {
           	System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
        
        saldo = Double.parseDouble(data[0].replace(',','.'));
    	salvar(c,"saldo",String.format("%.02f",saldo),false);
    	Acionista.setSaldo(saldo);
		
        if (acao.quantidade == 0)
            ativos.remove(acao.codigo);

        File a = new File(c + "ativos"); 
        a.delete();
        
        for (String aux : ativos.keySet()){
        	salvar(c,"ativos",(ativos.get(aux)).printF(),true);    		
        }

    	boolean type = Boolean.parseBoolean(data[4]);
    	int q = Integer.parseInt(data[5]);
    	double valor = Double.parseDouble(data[6].replace(',','.'));
    	
    	file.delete();

      	map_tid_estado.put(tid,"EFETIVADA");
    	System.out.println("--------------------------------------------------------");
        System.out.println("Caro cliente, A ação "+data[1]+" foi "+(type?"Vendida ":"Comprada ")+"com sucesso.");
        System.out.println("quantidade negociada: "+Integer.toString(q));
        System.out.println("valor transacionado: "+Double.toString(valor));
        System.out.println("--------------------------------------------------------");
    }
    void a_l(int tid) throws IOException{
    	lock(b_recurso,recurso,tid);
    	File temp = new File(c + "temporario"+Integer.toString(tid));
    	temp.delete();
    	map_tid_estado.put(tid,"ABORTADA");
    	salvar(c,"transacoes",Integer.toString(tid)+";ABORTADA\n",true);
    	
    	unlock(b_recurso);
    } 
    public void run() {
        while(true){
	        try {
	        	Task nova = (Task) tasks.take();
	        	if (nova.task == 0){
	        		e_l(nova.tid);
	        	}	
	        	else if (nova.task == 1 ){
	        		a_l(nova.tid);
	        	}
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	       	} catch (RemoteException e){
	       		e.printStackTrace();
	       	} catch (IOException e){
	       		e.printStackTrace();
	       	}
        }
    }
}
