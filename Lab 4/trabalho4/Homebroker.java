import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.text.*;
import java.rmi.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.Remote;
import java.rmi.server.UnicastRemoteObject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

// Lista de interesses dos clientes ok
// LISTA DE ORDENS DE COMPRA ok
// LISTA DE ORDENS DE VENDA ok 
// LISTA DE COTAÇÕES ok
// LISTA DE NOTIFICAÇÕES - REGISTRA O INTERESSE DOS CLIENTES EM RECEBEREM NOTIFICAÇÕES QUANDO DETERMINADO EVENTO ACONTECER 
		

public class Homebroker extends UnicastRemoteObject implements HomebrokerInt {
	static int novas_ordens; 
	static Map <Integer, String> map_tid_estado;
	static Map <String, AcionistaInt> msa; 						// msa = map -> string -> acionista
	static Map <String,List<String>> lista_de_interesses; 		// esta lista é usada para receber cotações de ações quando solicitado 
	static Map <String,Double> cotacoes;                        // lista com ação e cotação
	static Map <String,TreeSet<Ordem>> ordens;                  // lista que guarda ordens feitas pelo cliente, Ordens de Compra ou Ordens de venda
	static Map <String,TreeSet<Notificacao>> notificacoes; 		// lista que guarda notificaçoes do cliente com limite de ganho ou perda 
	static Boolean lock_cotacoes,lock_ordem,lock_notificacao; 	// variaveis que servem no controle de concorrencia
	static Queue fila_cotacoes,fila_ordem,fila_notificacao; 	// fila usada para gerenciar o controle de concorrencia
	static String path;
	static int tid;
	static BlockingQueue transacoes;
	static Coordenador gerenciador_de_transacao; 

	// inicializa variaveis usadas no homebroker citadas acima
	public Homebroker () throws RemoteException, IOException,InterruptedException{ 
		msa = new HashMap <String,AcionistaInt> ();
		lista_de_interesses = new HashMap <String,List<String>>(); 
		cotacoes = new HashMap <String,Double>();
		ordens = new HashMap <String,TreeSet<Ordem>>();
		notificacoes = new HashMap <String, TreeSet<Notificacao>>();
		map_tid_estado = new HashMap<Integer,String>();
		fila_notificacao = new LinkedList<>();
		fila_cotacoes = new LinkedList<>();
		fila_ordem = new LinkedList<>();
		lock_cotacoes = lock_ordem = lock_notificacao = false;
		transacoes = new LinkedBlockingQueue<>();		
		novas_ordens = 0;
		path = ".homebroker/";
		tid = 0;


		// pegar orderns de compra ou vendas já existentes
		File file = new File (path);
    	if (!file.exists()) file.mkdirs();
		gerenciador_de_transacao = new Coordenador(transacoes,map_tid_estado);
    	file = new File (path+"ordens");    	
	    try {
    		if (file.exists()){
	    		Scanner reader = new Scanner(file);
	    		while (reader.hasNextLine()){
	    			String d = reader.nextLine();	
	    			String[] data = d.split(";");
	    			SimpleDateFormat form = new SimpleDateFormat("dd-MM-yyyy-hh-mm");
	    			Date p = form.parse(data[4]);
					Ordem o = new Ordem(data[1],Integer.parseInt(data[2]),Double.parseDouble(data[3].replace(',','.')),p,data[5]);
					if (!ordens.containsKey(data[0]))
						ordens.put(data[0],new TreeSet<Ordem>(new Comp_Ordem()));
					if (data[6].equals("A")){
						//System.out.println(">> INSERE " + data[0]);
						if (ordens.get(data[0]).contains(o)){
							ordens.get(data[0]).remove(o);
						}
						ordens.get(data[0]).add(o);
					}
					else if (data[6].equals("F") && !ordens.get(data[0]).isEmpty()){
						//System.out.println(">> REMOVE " + data[0]);
						if (ordens.get(data[0]).contains(o)){
							ordens.get(data[0]).remove(o);
						}
						ordens.get(data[0]).add(o);
						if (o.quantidade == 0)
							ordens.get(data[0]).remove(o);
					}
	    		}
			}
		} catch (Exception e) {}
		for (String s : ordens.keySet()){
			System.out.println("Code : "+s);
			print(s);
		}
	}
	static public void salvar (String path, String name, String data) throws IOException {
    	salvar(path,name,data,false);
    }

	static public void salvar (String path, String name, String data, boolean b) throws IOException {
    	FileWriter  writer = new FileWriter(path +name,b);
    	PrintWriter gravar = new PrintWriter(writer);
    	gravar.printf(data);
    	writer.close();
    }

	// registra nova interface do acionista;
	public void registrar (AcionistaInt ai, String username ) throws RemoteException, InterruptedException{
		msa.put(username,ai);
		gerenciador_de_transacao.registraOnline(username);
		System.out.println(username + "atualizou seu ID");
		System.out.println(msa.size());
	}

	//Inserir cliente na lista de Interesses 
	public boolean inserirInteresse(String a,String codigo) throws RemoteException {
		if (!lista_de_interesses.containsKey(a))
			lista_de_interesses.put(a,new ArrayList<String>());
		if (!lista_de_interesses.get(a).contains(codigo))
			if (cotacoes.containsKey(codigo))
				return lista_de_interesses.get(a).add(codigo);
		return false;
	} 
	//Remover cliente na lista de Interesses 
	public boolean removerInteresse(String a,String codigo) throws RemoteException {
		if (lista_de_interesses.containsKey(a))
			return lista_de_interesses.get(a).remove(codigo);	
		return false; 
	} 
	// Insere ordem de compra ou venda na lista de ordens caso não haja nenhuma outra ordem que combine com a primeira 
	public boolean criarOrdem (String c, int q, double v, Date p, String a, boolean type) throws RemoteException, IOException { 
		Ordem o = new Ordem(c,q,v,p,a);
		boolean reply = true;
		lock(lock_ordem,fila_ordem,a);
		System.out.println(">> nova ordem");
		if (!achaMatch(o,type)){
			System.out.println(">> achou macth");
			String code_type = c + (type?"V":"C");
			if (!ordens.containsKey(code_type)){
				ordens.put(code_type,new TreeSet<Ordem>(new Comp_Ordem()));
			}
			try{
				reply = ordens.get(code_type).add(o);
				salvar(path,"ordens",code_type+";"+o.toString()+";A\n",true);
			}catch (IOException e){}
			
			print(c,type);
			unlock(lock_ordem);
		}
		else {
			unlock(lock_ordem);
			System.out.println("****Transação enviada*****");
			print(c,type);
		}
		return reply;
	}
	static public void setTid(int t){
		tid = t;
	}
	// Faz o match entre compra e venda e deixa todo mundo feliz 
	static public boolean achaMatch(Ordem o1 ,boolean type ) throws RemoteException, IOException { 
		String code_type = o1.codigo + (type?"C":"V"); // cria um código para procurar na lista correta 
		Queue temp = new LinkedList<Ordem>(); 
		if (ordens.containsKey(code_type)){
			while ((!ordens.get(code_type).isEmpty())){
				Ordem o2 = type?ordens.get(code_type).last():ordens.get(code_type).first(); 
				double vcliente = (o1.valor)*(type?-1:1);
				double vfila = 	  (o2.valor)*(type?-1:1);
				if((o1.quantidade<1)||(vcliente<vfila))
					return o1.quantidade == 0;

				Date today = new Date();
				if (today.compareTo(o2.prazo) <= 0 ){
					if (!o1.acionista.equals(o2.acionista)){
						double valor = (min (vcliente,vfila))*(type?-1:1); // ver depois << meter notificação
						int quantidade = min (o1.quantidade,o2.quantidade);
						iniciaTransacao(o1.acionista, o2.acionista, o1.codigo, quantidade,valor,type);
						o1.quantidade -= quantidade;
						o2.quantidade -= quantidade;
						if (o2.quantidade == 0){
							ordens.get(code_type).remove(o2);
							salvar(path,"ordens",code_type+";"+o2.toString()+";F\n",true);
						}
					}
					else {
						System.out.println(">> Entrou aqui 4");	
						temp.add(o2);
						ordens.get(code_type).remove(o2);
					}
				}
				else {

					System.out.println(">> Entrou aqui 5");
					ordens.get(code_type).remove(o2);	
					salvar(path,"ordens",code_type+";"+o2.toString()+";F\n",true);
				} 
			}
			while(!temp.isEmpty()){
				ordens.get(code_type).remove(temp.remove());
			}
			System.out.println(">> saiu 1");
		}
		return o1.quantidade == 0;
	} 
	static public void iniciaTransacao(String jamela1, String jamela2, String code, Integer quantidade, Double valor, Boolean type ) throws IOException{
		try {
			map_tid_estado.put(tid,"CRIADA");
			salvar(path,"estados",Integer.toString(tid)+";CRIADA\n",true);
			Transacao temp = new Transacao(tid, jamela1, jamela2, code, quantidade, valor, type);
			salvar(path,"transacoes",temp.toString(),true);
			transacoes.put(temp);
			tid++;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
	}
	// atualiza cotações depois de uma transação 
	static public void atualizaCotacoes(String c , Double v, String a){
		lock(lock_cotacoes,fila_cotacoes,a);
		Double v_antigo = cotacoes.get(c); 
		cotacoes.put(c,v);
		unlock(lock_cotacoes);
		Double sup = max(v_antigo,v);
		Double inf = min(v_antigo,v);
		lock(lock_notificacao,fila_notificacao,a); 
		if (notificacoes.containsKey(c)){
			Iterator<Notificacao> it = notificacoes.get(c).iterator();
			while (it.hasNext()){
				Notificacao n = it.next();
				if (n.v >= inf && n.v <= sup){
					pleaseNotify(n.a, "A acao "+c+" atingiu o valor "+ Double.toString(v) + ", o programado era "+ Double.toString(n.v));
					it.remove();
				}
				else if ( n.v > sup) break;
			}
		}
		unlock(lock_notificacao); 
	}
	// ******************************************************************************************************
	// Notifica cliente caso uma notificação programada seja atingida 
	static public void pleaseNotify(String a, String s){
		try{
			msa.get(a).notificarEventos(s);
		} catch (Exception e) {
            System.err.println("Homebroker exception: " + e.toString());
            e.printStackTrace();
        }
	}
	// ******************************************************************************************************
	// Notifica cliente quando ocorre uma transação
	// static public void pleaseNotify(String a, String c, int q ,double valor, boolean type){
	// 	try{
	// 		msa.get(a).notificarEventos(c,q,valor,type);
	// 	} catch (Exception e) {
	//      System.err.println("Homebroker exception: " + e.toString());
	//      e.printStackTrace();
	//  }
	// } 
	// obtem as cotações atuais da lista de cotações conforme o cliente 
	public String obterCotacoes( String a ) throws RemoteException { 
		lock(lock_cotacoes,fila_cotacoes,a);
		String ans = ""; 
		if (lista_de_interesses.containsKey(a)){
			ListIterator<String>  iterator = lista_de_interesses.get(a).listIterator(); 
	        while (iterator.hasNext()) { 
	            String acao = iterator.next();
	            ans += acao + " : "+Double.toString(cotacoes.get(acao))+"\n";
	 	    }
		}
		unlock(lock_cotacoes);
		return ans;
	} 	
	// Registra CLiente Na lista de Notificações com sua notificação
	public void registrarNotificacao(String a, String c, double l) throws RemoteException { 
		lock(lock_notificacao,fila_notificacao,a); 
		if (cotacoes.containsKey(c)){
			Notificacao n = new Notificacao(a,cotacoes.get(c) + l);
			if (!notificacoes.containsKey(c))
				notificacoes.put(c,new TreeSet<Notificacao>(new Comp_Notificacoes()));
			notificacoes.get(c).add(n);
		}
		unlock(lock_notificacao);
		System.out.println("Registrada a notificacao de "+ ((l<0.0)?"Perda":"Ganho") +" da ação "+c+" para o valor de "+Double.toString(cotacoes.get(c)+l));
	} 
	// lock algum recurso
	static public void lock ( Boolean item , Queue q, Object a){
		if (item == false)
			item = true;
		else{
			q.add(a);
			while ( item == true || a != q.peek());
			lock (item,q,a);
			q.remove();
		}
	}
	static public AcionistaInt ref (String code){
		return msa.get(code);
	}
	// unlock algum recurso
	static public void unlock (Boolean item){
		item = false;
	}
	// printa ordens quando chamadas
	public void print(String c , boolean type){
		String code_type = c+(type?"V":"C");
		print (code_type);
	} 
	public void print(String code_type){
		TreeSet<Ordem> p = ordens.get(code_type);
		if (p == null) return;
		System.out.println("--------------------------------------------------------");
		if (code_type.charAt(4) == 'V')
			System.out.println("Ordens de Venda:");
		else
			System.out.println("Ordens de Compra:");

		System.out.println("--------------------------------------------------------");
		for (Ordem o : p){
			o.printOrdem();
		}
		System.out.println("--------------------------------------------------------");
	}
	static double min (double a, double b){
		return a<b?a:b;
	}
	static Double min (Double a, Double b){
		return a<b?a:b;
	}
	static double max (Double a, Double b){
		return a>b?a:b;
	}
	static int min (int a, int b){
		return a<b?a:b;
	}
	public String obterEstadoTransacao (int tid) throws RemoteException{
		return gerenciador_de_transacao.obterEstadoTransacao(tid);
	}

	// ** atrubuir aleatório apoenas 1 vez
	public static void main (String[] args) throws RemoteException {
		int MAX = 1048575; 
		try{
			HomebrokerInt h = new Homebroker();
			Registry registry = LocateRegistry.createRegistry(10099);
			registry.bind("MarRicoHomebroker",h);
			// cria ações aleatórias de A000 a A020 com preços de cotação aletorios
			Random r = new Random();
			String code;
			for (int i = 0; i < 20; i++){
				code = "A0"+String.format("%02d",i);
				cotacoes.put(code,(double)(((r.nextInt()&MAX)%100)));
    		}
		}catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
        new Thread(gerenciador_de_transacao).start();
	}
}
// Classe que representa um objeto de notificação
class Notificacao {
	public String a;
	public double v;
	public Notificacao (String ac, double vc){
		this.a = ac;
		this.v = vc;
	}
	public boolean equal(Notificacao n){
		return ((this.a == n.a)&&(this.v == n.v));	 
	}
}
// Classe que é usada como comparadora de objetos de notificaçao
class Comp_Notificacoes implements Comparator<Notificacao> {
	public int compare (Notificacao n1, Notificacao n2){ // se n2 deve vir antes de n1 retorna positivo
		Notificacao first = n1;
		Notificacao second = n2;
		if (n1.equal(n2)) return 0;
		return second.v<=first.v?1:-1;
	}
}
// Classe que representa um objeto de uma ordem
class Ordem {
	public String codigo;
	public int quantidade;
	public double valor;
	public Date prazo;
	public String acionista;
	
	public String toString(){
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm");  
		String strDate = dateFormat.format(this.prazo);
		return codigo+";"+Integer.toString(quantidade)+";"+String.format("%.02f",valor)+";"+strDate+";"+acionista;
	}
	public void printOrdem( ){
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm");  
		String strDate = dateFormat.format(this.prazo);
		System.out.print(this.codigo);
		System.out.print(" ");
		System.out.print(this.quantidade);
		System.out.print(" ");
		System.out.print(this.valor);
		System.out.print(" ");
		System.out.println(strDate);		
	}
	public Ordem (String c, int q, double v, Date p, String a){
		this.codigo = c;
		this.quantidade = q;
		this.valor = v;
		this.prazo = p;
		this.acionista = a;
	}
	public boolean equal(Ordem o){
		return ((this.codigo.equals(o.codigo))&&
			    //(this.quantidade == o.quantidade)&&
				(this.valor == o.valor)&&
				(this.prazo.compareTo(o.prazo) == 0)&&
				(this.acionista.equals(o.acionista)));	 
	}
}
// Classe que compara ordens para que possamos ordena-las
class Comp_Ordem implements Comparator<Ordem> {
	public int compare (Ordem o1, Ordem o2){ // se o2 deve vir antes de o1 retorna positivo
		Ordem first = o1;
		Ordem second = o2;
		if (o1.equal(o2)) return 0;
		return second.valor<=first.valor?1:-1;
	}
}

class Transacao {
	Integer quantidade,tid; 
	String a,b,code;
	Double valor;
	Boolean type;
	public Transacao (Integer tid,String ac1, String ac2, String code,Integer quantidade, Double valor, Boolean type){
		this.tid = tid;
		this.a = ac1;
		this.b =  ac2;
		this.code = code;
		this.quantidade = quantidade;
		this.valor = valor;
		this.type = type;
	}
	public String toString(){
		return tid.toString()+";"+a+";"+b+";"+code+";"+quantidade.toString()+";"+valor.toString()+";"+type.toString()+";\n";
	}
}


class Coordenador extends UnicastRemoteObject implements Runnable {
 	
 	protected Map<String,List<Transacao>> temp_usr_tran = new HashMap<String,List<Transacao>>();
   	protected Map<Transacao,Integer> temp_tran_userOn= new HashMap<Transacao,Integer>();

    protected BlockingQueue queue = null;
    protected Map<Integer,String> map = null;
    String path = ".homebroker/";

    public Coordenador(BlockingQueue queue , Map map )throws RemoteException, IOException, InterruptedException{
        this.queue = queue;
        this.map = map;
        File f = new File(path + "estados");
        if (f.exists()){
		    Scanner reader = new Scanner(f);
		   	while (reader.hasNextLine()){
				String d = reader.nextLine();	
				String[] data = d.split(";");
				map.put(Integer.parseInt(data[0]),data[1]);
		    	Homebroker.setTid(Integer.parseInt(data[0])+1);		
		    }
        }
        f = new File(path + "transacoes");
        if (f.exists()){
	     	Scanner reader = new Scanner(f);
	        while (reader.hasNextLine()){
				String d = reader.nextLine();	
				String[] data = d.split(";");
				Integer i = Integer.parseInt(data[0]);
		    	if (!map.get(i).equals("EFETIVADA") && !map.get(i).equals("ABORTADA")){
					Transacao temp = new Transacao(i, data[1], data[2], data[3], Integer.parseInt(data[4]), Double.parseDouble(data[5]), Boolean.parseBoolean(data[6]));
					if (!temp_tran_userOn.containsKey(temp))
						temp_tran_userOn.put(temp,0);
					if (!temp_usr_tran.containsKey(data[1])){
						temp_usr_tran.put(data[1],new LinkedList<Transacao>());
						temp_usr_tran.get(data[1]).add(temp);
					}
					if (!temp_usr_tran.containsKey(data[2])){
						temp_usr_tran.put(data[2],new LinkedList<Transacao>());
						temp_usr_tran.get(data[2]).add(temp);
					}
					System.out.println("Transacao aberta encontrada >> " + data[1]+ ">>" + data[2]);
				}		    
		    }	
        }
    }

    void abreTransacao(Transacao t)throws RemoteException, IOException , InterruptedException{
    	map.put(t.tid,"ABERTA");
		salvar(path,"estados",t.tid.toString()+";ABERTA\n",true);
			
    	System.out.println(">> A transacão foi aberta");
    	AcionistaInt a = Homebroker.ref(t.a);
    	AcionistaInt b = Homebroker.ref(t.b);
    	a.abreTransacao(t.tid); 
    	b.abreTransacao(t.tid);
    	System.out.println(">> A transaão foi enviada para clientes");
    	boolean voto_a = a.preparar(t.tid,t.code,t.quantidade,t.valor,t.type);
    	boolean voto_b = b.preparar(t.tid,t.code,t.quantidade,t.valor,!t.type);
    	boolean votos = voto_a && voto_b;

    	System.out.println(">> Clientes votaram");
    	if (votos) {
    		System.out.println(">> Transação efetivada");
			salvar(path,"estados",t.tid.toString()+";EFETIVADA\n",true);
		
			Homebroker.atualizaCotacoes(t.code,t.valor,t.a); 
    		map.put(t.tid,"EFETIVADA");
    		a.efetivar(t.tid);
    		b.efetivar(t.tid);
    	}
    	else{
    		System.out.println(">> Transação abortada");
    		map.put(t.tid,"ABORTADA");
 	   		salvar(path,"estados",t.tid.toString()+";ABORTADA\n",true);		
    		a.abortar(t.tid);
    		b.abortar(t.tid);
    		// recover Ordem
    	}
    }	



    void registraOnline(String codigo) throws InterruptedException{
	    if (temp_usr_tran.containsKey(codigo)){
	    	for (Transacao t : temp_usr_tran.get(codigo)){
	    		Integer on = temp_tran_userOn.get(t);on++;
	    		System.out.println(">> Acionistas online >> " +on.toString());
	    		if (on >= 2){
	    			queue.put(t);
	    			temp_tran_userOn.remove(t);
	    			temp_usr_tran.get(codigo).remove(t);
	    		}
	    		else {
	    			temp_tran_userOn.put(t,on);
	    		}
	    	}
    	}
    }

    String obterEstadoTransacao (int tid){
    	return map.get(tid);
    }

    static public void salvar (String path, String name, String data) throws IOException {
    	salvar(path,name,data,false);
    }

	static public void salvar (String path, String name, String data, boolean b) throws IOException {
    	FileWriter  writer = new FileWriter(path +name,b);
    	PrintWriter gravar = new PrintWriter(writer);
    	gravar.printf(data);
    	writer.close();
    }


    public void run() {
        while(true){
	        try {
	        	Transacao nova = (Transacao) queue.take();
	        	abreTransacao(nova);
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	       	} catch (RemoteException e){
	       		e.printStackTrace();
	       	} catch (IOException e){
	       		e.printStackTrace();
	       	}
        }
    }
}


