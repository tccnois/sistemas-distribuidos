import java.io.*;
import java.net.*;
import java.util.*;
import java.math.*;
import java.security.*;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;

public class MulticastPeer{
	
    public static void main(String args[]) throws NoSuchAlgorithmException,InvalidKeyException, SignatureException{ 
		// args give message contents and destination multicast group (e.g. "228.5.6.7")
		
		MulticastSocket s =null;
		try {
 			// Pega nome do usuário
			Scanner mscanner = new Scanner(System.in); 
			System.out.print(">> Nome de Usuário:\n>> ");
			String m = null, username = mscanner.nextLine();

	        //Geração das chaves públicas e privadas
			Signature sig = Signature.getInstance("DSA");
	        KeyPairGenerator kpg = KeyPairGenerator.getInstance("DSA");
	        SecureRandom secRan = new SecureRandom();  
	        kpg.initialize(512,secRan);  
	        KeyPair    keyP = kpg.generateKeyPair();  
	        PublicKey  pubKey = keyP.getPublic();  
	        PrivateKey priKey = keyP.getPrivate();

			// coloca a chave privada no criptografador 
			sig.initSign(priKey); 

			// entra no grupo através do ip fornecido por arg 0
			InetAddress group = InetAddress.getByName(args[0]);
			s = new MulticastSocket(6789);
			s.joinGroup(group);

			// instancia o lsitener 
			Listener listener = new Listener(s,group,username);

			// manda a primeira mensagem pra avisar todo mundo que o usuário entrou no grupo
			// estrutura -> tag fist message <> chave publica <> tamanho da chavepublica em bytes <> nome de usuario <> 
			pleaseSend(pleaseConcatenate(pleaseConcatenate(("fm<>").getBytes(),pubKey.getEncoded()),("<>"+Integer.toString(pubKey.getEncoded().length)+"<>"+username+"<>").getBytes()),s,group);

			int idmensagem = 0 ;
 			do{	// Send messages to others
 				m = mscanner.nextLine();
 				idmensagem++; 
			 	sig.update(m.getBytes()); // assina o md5 da mensagem
			 	byte[] assinatura = sig.sign();
				pleaseSend(pleaseConcatenate(pleaseConcatenate("ms<>".getBytes(),assinatura),("<>"+Integer.toString(assinatura.length)+"<>"+username+"<>"+m+"<>"+Integer.toString(idmensagem)+"<>").getBytes()),s,group); // 0-tag 1-assinatura 2-tamanho 3-username 4-mensagem
				// tag message <> assinatura digital <> tamanho da assinatura <> nome de usuario <> mensagem <>
  			}while (!m.startsWith("!sair"));
			
			s.leaveGroup(group);		
			
			System.exit(0);
		
		}catch (SocketException e){System.out.println("Socket: " + e.getMessage());
		}catch (IOException e){System.out.println("IO: " + e.getMessage());
		}finally {if(s != null) s.close();}
	}
	public static void pleaseSend(byte [] b,MulticastSocket s,InetAddress group){
		try{
			DatagramPacket messageOut = new DatagramPacket(b, b.length, group, 6789);
			s.send(messageOut);	
		} catch (EOFException e){System.out.println("EOF:"+e.getMessage());
		} catch(IOException e) {System.out.println("readline:"+e.getMessage());}
	}
	public static byte [] pleaseConcatenate( byte [] m1, byte [] m2 ){ // Essa função concatena dois vetores de bytes 
		byte[] mensagem = new byte[m1.length + m2.length];
		System.arraycopy(m1, 0, mensagem, 0, m1.length);
		System.arraycopy(m2, 0, mensagem, m1.length,m2.length);
		return mensagem;
	}		      		
}

class Pair <X, Y> { 
  public final X first; 
  public final Y second;
  public Pair(X x, Y y) { 
    this.first = x; 
    this.second = y; 
  } 
}

class Listener extends Thread {
	MulticastSocket clientSocket;
	InetAddress group;
	String username;
	Map <String,Pair<PublicKey,HashMap<String,Pair<Boolean,Pair<Boolean,Pair<Integer,Integer>>>>>> usersInfo; 
    // nome <> chave publica <> Map de mensagens < identificador da mensagem <> veracidade <> autorizacao para votar <> numero de pessoas no momento que a mensagem foi enviada <> numero de denuncias >
	public Listener (MulticastSocket aClientSocket,InetAddress agroup,String ausername) { 
		usersInfo = new HashMap<String,Pair<PublicKey,HashMap<String,Pair<Boolean,Pair<Boolean,Pair<Integer,Integer>>>>>>();
		clientSocket = aClientSocket;
		group = agroup;
		username = ausername;
		this.start();	
	}

	public void run(){
		while(true){
			try {	

				byte[] buffer = new byte[1000];
				DatagramPacket messageIn = new DatagramPacket(buffer, buffer.length);
 				clientSocket.receive(messageIn);

 				byte[] saidaBytes = messageIn.getData();
				String[] saida = new String(saidaBytes).split("<>");
 				
 				if (saida[0].startsWith("fm")){ // trata a primeira mentagem de cada usuario <fm = first message>
					setDictionary(saida[3],getPublicKey(saidaBytes,new Integer(saida[2]))); 						
					System.out.println("------------------------------------------------------\n"+">> "+saida[3]+" se juntou ao grupo"+"\n------------------------------------------------------");
					if (!saida[3].equals(username)){ 
						byte[] replyDataUser =pleaseConcatenate(pleaseConcatenate(("rm<>").getBytes(),getPKeyUsersInfoB(username)),("<>"+getPKeySize(username)+"<>"+username+"<>"+"<>").getBytes());
	 					DatagramPacket reply = new DatagramPacket(replyDataUser,replyDataUser.length,messageIn.getAddress(), messageIn.getPort());
	    				clientSocket.send(reply);

	    				HashMap<String,Pair<Boolean,Pair<Boolean,Pair<Integer,Integer>>>>h = getMessagesHist(username);
						for (String id:h.keySet()){
		    				byte[] replyDataUserHist = ("hm<>"+username+"<>"+id+"<>"+Boolean.toString(h.get(id).first)+"<>false<>"+Integer.toString(getAuthorizedUsers(username,id))+"<>"+Integer.toString(getDenounceNumber(username,id))+"<>").getBytes();
		    				DatagramPacket replyHist = new DatagramPacket(replyDataUserHist,replyDataUserHist.length,messageIn.getAddress(), messageIn.getPort());
		    				clientSocket.send(replyHist);
	    				}
					}
 				}
 				else if (saida[0].startsWith("rm")){ // <rm = reply message> mensagem de resposta a "first message"
 					setDictionary(saida[3],getPublicKey(saidaBytes,new Integer(saida[2])));						
					System.out.println("------------------------------------------------------\n"+">> " + saida[3] + " te enviou sua chave publica"+"\n------------------------------------------------------");
 				}
 				else if (saida[0].startsWith("ms") && saida[4].startsWith("!sair")){ // ms = message 
 					System.out.println(">> "+saida[3]+" saiu do grupo");
 					usersInfo.remove(saida[3]);
 				}
 				else if (saida[0].startsWith("ms") && saida[4].startsWith("!fake") && (saida[3].equals(username))){   
 					String[] denuncia = saida[4].split(" "); //!fake usuario id
 					if (getAuthorization(denuncia[1],denuncia[2])){
						System.out.println(">> Enviando denuncia para a rede");
						byte[] replyDataUser =("fk<>"+username+"<>"+denuncia[1]+"<>"+denuncia[2]+"<>").getBytes();
	 					DatagramPacket reply = new DatagramPacket(replyDataUser,replyDataUser.length,group, messageIn.getPort());
	    				clientSocket.send(reply);
	    				
					}
					else
						System.out.println(">> Você não tem autorização para votar essa mensagem");
 				}
 				else if (saida[0].startsWith("ms") && !saida[4].startsWith("!fake")){ // <ms = message> 0-tag 1-assinatura 2-tamanho 3-username 4-mensagem 5-idMensagem 
					if(!verifySignature(saida[3],saida[4],getSignature(saidaBytes,new Integer(saida[2])),saida[5]) & username.equals(saida[3])){
						System.out.println(">> minha pessoa, deu um erro na sua assinatura, mande sua mensagem again por gentilesa!");
					}
 				}
 				else if (saida[0].startsWith("fk")){ // alguém mandou um fake então setados a denuncia dentro da nossa ficha
 					System.out.println(">> Gente do céu, "+saida[1]+" denunciou como fake a mensagem "+saida[3]+" da pessoa "+saida[2]);
 					setDenounce(saida[2],saida[3]);
 				}
 				else if (saida[0].startsWith("hm")){ // # manda histórico de mensagens para saber a reputação dos usuários antigos
 					setMessageHist(saida[1],saida[2],Boolean.parseBoolean(saida[3]),Boolean.parseBoolean(saida[4]),Integer.parseInt(saida[5]),Integer.parseInt(saida[6]));
 					
 				}
			} catch (EOFException e){System.out.println("EOF:"+e.getMessage());
			} catch(IOException e) {System.out.println("readline 2:"+e.getMessage());System.exit(0);}
		}
	}
	public static byte [] pleaseConcatenate( byte [] m1, byte [] m2 ){ // This function contatenate two byte arrays
		byte[] mensagem = new byte[m1.length + m2.length];
		System.arraycopy(m1, 0, mensagem, 0, m1.length);
		System.arraycopy(m2, 0, mensagem, m1.length,m2.length);
		return mensagem;
	}


	// ***************************************usersInfo Series******************************************
	byte [] getPKeyUsersInfoB(String nome){ // # retorna a chave pública da estrutura em vetor de bytes 
		return usersInfo.get(nome).first.getEncoded();
	}
	PublicKey getPKeyUsersInfo(String nome){ // # retorna a chave pública da estrutura userinfo em string
		return usersInfo.get(nome).first;
	}
	String getPKeySize(String nome){ // # retorna o tamanho da chave publica do usuário
		return Integer.toString(usersInfo.get(nome).first.getEncoded().length);
	}
	// devolve um histórico completo de um usuário
	HashMap<String,Pair<Boolean,Pair<Boolean,Pair<Integer,Integer>>>> getMessagesHist (String nome){
		return usersInfo.get(nome).second;
	}
	int getNumberUsers(){
		return usersInfo.size();
	}
	// preenche a ficha de histórico
	void setMessageHist(String nome,String idmensagem,boolean veracidade,boolean autorizacao_para_votar,int pessoas_na_rede ,int numero_de_denuncias ){ 
		HashMap<String,Pair<Boolean,Pair<Boolean,Pair<Integer,Integer>>>>historico = getMessagesHist(nome);
		historico.put(idmensagem,new Pair(veracidade,new Pair(autorizacao_para_votar,new Pair(pessoas_na_rede,numero_de_denuncias)))); // 0 representa o numero de denuncias da mensagem;
	}
	// conta o numero de mensagens fakes de um usuario
	int getMessagesFakes (String nome){ 
		int fakes = 0;
		HashMap<String,Pair<Boolean,Pair<Boolean,Pair<Integer,Integer>>>> historico = getMessagesHist(nome);
		for (String idmensagem : historico.keySet())
			if (historico.get(idmensagem).first==false)
				fakes++; 
		return fakes;
	}
	// conta o numero de mensagens que um usuário enviou
	int getNumberMessages (String nome){
		return getMessagesHist(nome).size();
	}
	// faz o calculo da reputação da pessoa 
	String getReputation(String nome){
		int mensagens_enviadas = getNumberMessages(nome);
		int mensagens_fakes = getMessagesFakes(nome);
		return String.format("%.2f",100.0f*(1.0f- new Float(mensagens_fakes)/new Float(mensagens_enviadas)));
	}
	// verifica se a pessoa tem autorização pra votar numa mensagem
	boolean getAuthorization(String faker,String id){
		HashMap<String,Pair<Boolean,Pair<Boolean,Pair<Integer,Integer>>>>historico = getMessagesHist(faker);
		return historico.get(id).second.first;
	}
	// numero de denuncias ne uma mensagem
	int getDenounceNumber(String user, String id){
		HashMap<String,Pair<Boolean,Pair<Boolean,Pair<Integer,Integer>>>>historico = getMessagesHist(user);
		return historico.get(id).second.second.second;
	} 
	// conta numero de usuarios autorizados a votar numa mensagem
	int getAuthorizedUsers(String user, String id){
		HashMap<String,Pair<Boolean,Pair<Boolean,Pair<Integer,Integer>>>>historico = getMessagesHist(user);
		return historico.get(id).second.second.first;
	}
	// verifica se mensagem pode ser considerada falsa
	void setDenounce(String faker,String id){
		HashMap<String,Pair<Boolean,Pair<Boolean,Pair<Integer,Integer>>>>historico = getMessagesHist(faker);
		int denuncias = getDenounceNumber(faker,id)+1;
		int usuarios_autorizados = getAuthorizedUsers(faker,id);
		boolean autorizacao = historico.get(id).second.first;
		boolean veracidade = historico.get(id).first;
		
		if ((usuarios_autorizados/2)<denuncias){
			veracidade = false;		
		}
		historico.replace(id,new Pair(veracidade,new Pair(autorizacao,new Pair(usuarios_autorizados,denuncias))));
	}
	// **************************************************************************************************


	byte [] getPublicKey(byte[] m , int tam){ // Get the public key from the bytes array
		byte [] mensagem = new byte[tam];
		System.arraycopy(m, 4, mensagem, 0,tam);	
		return mensagem;
		// corrompe mensagem[tam/2]=0;
	}
	void setDictionary (String nome,byte [] pubKey){ // # Inicializa ficha de um usuario
		try{
			/* nome<>chave publica <> 
		   	   Map de mensagens < identificador da mensagem <> veracidade <> autorizacao para votar <> numero de pessoas no momento que a mensagem foi enviada <> numero de denuncias >
		       Map<Integer,Pair<Boolean,Pair<Boolean,Pair<Integer,Integer>>>> */

			KeyFactory keyFactory = KeyFactory.getInstance("DSA");    
			EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(pubKey);
			PublicKey publicKey2 = keyFactory.generatePublic(publicKeySpec);
		 	usersInfo.put(nome,new Pair(publicKey2,new HashMap<String,Pair<Boolean,Pair<Boolean,Pair<Integer,Integer>>>>()));
		}catch(NoSuchAlgorithmException e){System.out.println("DSA Error");
		}catch(InvalidKeySpecException e){System.out.println("InvalidKeySpecException"+e.getMessage());}					
	}
	byte [] getSignature(byte[] m , int tam){ // Pega a assinatura de uma mensagem em bytes
		byte [] mensagem = new byte[tam];
		System.arraycopy(m, 4, mensagem, 0,tam);	
		return mensagem;
	}

	boolean verifySignature(String nome, String mensagem, byte[] assinatura,String idmensagem){ // verifica a assinatura, se for bem sucedida printa a mensagem pra em todas as contas, senão não printa
       try{
			Signature clientSig = Signature.getInstance("DSA");  
	       	clientSig.initVerify(getPKeyUsersInfo(nome));  
	       	
	       	clientSig.update(mensagem.getBytes());  
		    if (clientSig.verify(assinatura,0,46)) {  //Mensagem corretamente assinada
	        	setMessageHist(nome,idmensagem,true,true,getNumberUsers(),0);
	        	System.out.println("------------------------------------------------------\n"+nome+" ("+getReputation(nome)+"%): "+mensagem+" ");
		        System.out.println("<id "+idmensagem+"> <Assinatura Validada>\n------------------------------------------------------");
		    } else { //Mensagem não pode ser validada
		        System.out.println("------------------------------------------------------\n"+nome+": "+mensagem+" ");
		        System.out.println("<Assinatura NÃO Validada>\n------------------------------------------------------");
		    }

		    return true;
       }catch(NoSuchAlgorithmException e){ System.out.println("Error 1:"+e.getMessage());
       }catch(InvalidKeyException e){ System.out.println("Error 2: "+e.getMessage());
       }catch(SignatureException e){ //System.out.println("Error 3: "+e.getMessage());
       }
       return false;
   }


}