import java.rmi.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ServImpl extends UnicastRemoteObject implements InterfaceServ {
	public ServImpl () throws RemoteException{}

	public void chamar (String s , InterfaceCli c)throws RemoteException{
		c.echo(s);
	}

	public static void main(String[] args)throws RemoteException {
		try {
			ServImpl s = new ServImpl();
			Registry registry = LocateRegistry.createRegistry(10099);
			registry.bind("JamelaServer",s);
			System.out.println("JamelaReady");
		} catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
	}
}

// java -classpath Servidor_HelloWorld/HelloWorld/ -Djava.rmi.server.codebase=file:Servidor_HelloWorld/HelloWorld/ ServImpl &