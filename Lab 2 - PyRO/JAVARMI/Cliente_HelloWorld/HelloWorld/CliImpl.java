import java.rmi.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.Remote;
import java.rmi.server.UnicastRemoteObject;


public class CliImpl extends UnicastRemoteObject implements InterfaceCli{
	public CliImpl() throws RemoteException{}

	public void echo (String s)throws RemoteException{
		System.out.println(s);
	}

	public static void main(String[] args)throws RemoteException{
        try {
			InterfaceCli c = new CliImpl();	
            Registry registry = LocateRegistry.getRegistry(10099);
            InterfaceServ s = (InterfaceServ) registry.lookup("JamelaServer");
            s.chamar("Funfa",c);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }

	}
}

//java -classpath Cliente_HelloWorld/HelloWorld/ CliImpl