import java.util.Date;
import java.rmi.*;
import java.rmi.RemoteException;
import java.rmi.Remote;

public interface HomebrokerInt extends Remote{
	public boolean inserirInteresse(String a,String codigo) throws RemoteException ; // << Inserir cliente na lista de Interesses
	public boolean removerInteresse(String a,String codigo) throws RemoteException ; // << Remover cliente na lista de Interesses  
	public boolean criarOrdem ( String c, int q, double v, Date p, String a, boolean type) throws RemoteException ; // << Insere ordem de compra na lista de ordens de compra || 0 para compra e 1 para venda
	public String obterCotacoes(String a) throws RemoteException ; // << obtem as cotações atuais da lista de cotações 
	public void registrarNotificacao(String a, String c, double l) throws RemoteException ; // << Registra CLiente Na lista de Notificações com sua notificação	
}