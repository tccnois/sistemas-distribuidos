import java.rmi.*;
import java.rmi.RemoteException;
import java.rmi.Remote;

public interface AcionistaInt extends Remote {
	public void notificarEventos(String not ) throws RemoteException;	
	public void notificarEventos(String code, int q ,double valor, boolean type) throws RemoteException;	
}

// [^a-z\t\-\,\"\'\+@\n.\(\) \<\>\*\&0-9\{\}\=\:\#\/\[\]\_\|\!]