from werkzeug.datastructures import ImmutableMultiDict
from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps
from random import randint as ri 
from random import uniform as rd 
import requests
import thread
import time

c = input("selecione porta: ")
server = "http://localhost:8080/"
saldo = rd(100.0,10000.0) 	
ativos = {}

class Acao:
	def __init__(self, codigo, quantidade, pm):
		self.codigo = codigo
		self.quantidade = quantidade
		self.pm = pm
	
	def printa(self):
		print "codigo: "+self.codigo+"| quantidade: "+str(self.quantidade)+"| preco medio: "+"{:.2f}".format(self.pm)

#-------------------------------------------
app = Flask(__name__)

@app.route('/notificarEvento1',methods = ['POST'])
def notificarEvento1():
	print("--------------------------------------------------------")
	print("[ALERTA PROGRAMADO]")
	var = request.json.get('ms')
	print(var)
	print("--------------------------------------------------------")
	return "OK"

@app.route('/notificarEvento2/<code>&<q>&<valor>&<tp>',methods = ['POST'])
def notificarEvento2(code,q,valor,tp):
	typ = (tp == 'true')
	print("--------------------------------------------------------")
	print("Caro cliente, A acao "+code+" foi "+("Vendida " if typ else "Comprada ")+"com sucesso.")
	print("quantidade negociada: "+q)
	print("valor transacionado: "+valor)
	print("--------------------------------------------------------")
	q = int(q)
	valor = float(valor)
	global saldo 
	saldo += valor*(1.0 if typ else -1.0)*float(q)
	if (ativos.get(code) == None):
		ativos[code] = Acao(code,0,0)
	acao = ativos.get(code)
	if (typ == False):
		inserirInteresse(c,code) # quando acao eh comprada insere interesse na lista de interesses 
		acao.pm= (acao.pm*acao.quantidade + valor*q)/(acao.quantidade+q)
		
	acao.quantidade += q*(-1 if typ else 1)
	
	if (acao.quantidade == 0):
		ativos.pop(acao.codigo)
	return "OK"

def req():
	app.run(host = 'localhost', port = c)

try:
	thread.start_new_thread( req, ())
except:
	print 'Error: unable to start thread'
#-------------------------------------------
def inserirInteresse(a, codigo): #  // << Inserir cliente na lista de Interesses
	r = requests.post(server+'inserirInteresse/'+str(a)+'&'+codigo)
	return r

def removerInteresse(a, codigo) : # // << Remover cliente na lista de Interesses  
	r = requests.delete(server+'removerInteresse/'+str(a)+'&'+codigo)
	return r
	
def criarOrdem (q, v, type, c, a, d): # // << Insere ordem de compra na lista de ordens de compra || 0 para compra e 1 para venda
	r = requests.post(server+'criarOrdem/'+str(q)+'&'+str(v)+'&'+str(type)+'&'+c+'&'+str(a)+'&'+d)
	return r

def obterCotacoes(a):# // << obtem as cotacoes atuais da lista de cotacoes 
	r = requests.get(server+'obterCotacoes/'+str(a))
	return r.content

def registrarNotificacao(a, c, l): # // << Registra CLiente Na lista de Notificacoes com sua notificacao	
	requests.post(server+'registraNotificacao/'+str(a)+'&'+c+'&'+str(l))

#-------------------------------------------

def validv(q,valor):
	return saldo >= q*valor
	
def validc(q,codigo):
	if (ativos.get(codigo) != None):
		return ativos.get(codigo).quantidade >= q
	return false

def mensagem(m):
	print("Mensagem------------------------------------------------")
	print(m)
	print("--------------------------------------------------------")


def init():
	for i in range(20):
		if (ri(0,1) == 1):
			code = 'A0' + "{:02d}".format(i)
			ativos[code] = Acao(code,ri(1,100),rd(10.30,100.50))
			inserirInteresse(c,code)

init()

op = '8'
while(op != '0'):
	print("0 - Para sair")
	print("1 - Obter cotacoes")
	print("2 - Criar ordem de compra")
	print("3 - Criar ordem de venda")
	print("4 - registrar interesse")
	print("5 - remover interesse")
	print("6 - registrar alerta a limite de ganho ou perda")
	print("7 - Ver minha carteira")
	op = raw_input()
	if (op == '1'):
		print(obterCotacoes(c))
	elif (op == '2'):
		codigo = raw_input("Codigo da acao: ")
		q = int(raw_input("Quantidade: "))
		v = float(raw_input("Preco (use ponto): "))
		d = raw_input("Data de validade da ordem (dd-mm-yyyy-hh-mm): ")
		if (validv(q,v)):
			if (criarOrdem(q,v,False,codigo,c,d)):
				mensagem("Ordem de compra enviada com sucesso")
			else:
				mensagem("Falha na operacao, tente novamente")
		else:
			mensagem("Voce nao tem saldo suficiente")
		
	elif (op == '3'):
		codigo = raw_input("Codigo da acao: ")
		q = int(raw_input("Quantidade: "))
		v = float(raw_input("Preco (use ponto): "))
		d = raw_input("Data de validade da ordem (dd-mm-yyyy-hh-mm): ")
		if (validc(q,codigo)):
			if (criarOrdem(q,v,True,codigo,c,d)):
				mensagem("Ordem de venda enviada com sucesso")
			else:
				mensagem("Falha na operacao, tente novamente")
		else:
			mensagem("Voce nao tem a quantidade de acoes necesarias para efetuar a venda")

	elif (op == '4'):
		codigo = raw_input("Codigo da acao: ")
		if (inserirInteresse(c,codigo)):
			mensagem("Interesse inserido com sucesso")
		else: 
			mensagem("Falha na operacao, tenta novamente")
		
	elif (op == '5'):
		codigo = raw_input("Codigo da acao: ")
		if (removerInteresse(c,codigo)):
			mensagem("Interesse Removido")
		else:
			mensagem("Falha na operacao, tenta novamente")
	elif (op == '6'):
		codigo = raw_input("Codigo da acao: ")
		l = float(raw_input("Limite de ganho(digite um valor positivo) ou perda(digite um valor negativo): "))
		registrarNotificacao(c, codigo, l)
	elif (op == '7'):
		print("Carteira------------------------------------------------")
		print("Saldo:"+"{:.2f}".format(saldo))
		print("Ativos:")
		for key in ativos:
			ativos.get(key).printa()
		print("--------------------------------------------------------")
	else :
		print("Entrada INCORRETA!")



