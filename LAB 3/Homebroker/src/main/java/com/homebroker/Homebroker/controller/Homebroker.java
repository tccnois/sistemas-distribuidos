package com.homebroker.Homebroker.controller;


import java.io.OutputStream;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.util.*;
import java.text.*;

@RestController
public class Homebroker {
	static int novas_ordens; 
	static Map <String,List<String>> lista_de_interesses; // esta lista é usada para receber cotações de ações quando solicitado 
	static Map <String,Double> cotacoes;                        // lista com ação e cotação
	static Map <String,TreeSet<Ordem>> ordens;                  // lista que guarda ordens feitas pelo cliente, Ordens de Compra ou Ordens de venda
	static Map <String,TreeSet<Notificacao>> notificacoes; 		// lista que guarda notificaçoes do cliente com limite de ganho ou perda 
	static Boolean lock_cotacoes,lock_ordem,lock_notificacao; 	// variaveis que servem no controle de concorrencia
	static Queue fila_cotacoes,fila_ordem,fila_notificacao; 	// fila usada para gerenciar o controle de concorrencia

	// inicializa variaveis usadas no homebroker citadas acima
	public Homebroker () { 
		lista_de_interesses = new HashMap <String,List<String>>(); 
		cotacoes = new HashMap <String,Double>();
		ordens = new HashMap <String,TreeSet<Ordem>>();
		notificacoes = new HashMap <String, TreeSet<Notificacao>>();
		fila_notificacao = new LinkedList<>();
		fila_cotacoes = new LinkedList<>();
		fila_ordem = new LinkedList<>();		
		lock_cotacoes = lock_ordem = lock_notificacao = false;
		novas_ordens = 0;
		try{
			int MAX = 1048575;
			Random r = new Random();
			String code;
			for (int i = 0; i < 20; i++){
				code = "A0"+String.format("%02d",i);
				cotacoes.put(code,(double)(((r.nextInt()&MAX)%100)));
	    	}
		}catch (Exception e) {
	        System.err.println("Server exception: " + e.toString());
	        e.printStackTrace();
	    }
	}
	//@GetMapping("/hello")
	//public String hello() {
	//	return "Hello";
	//}
	//Inserir cliente na lista de Interesses 
	@PostMapping("/inserirInteresse/{a}&{codigo}")
	public boolean inserirInteresse(@PathVariable(value = "a") String a,@PathVariable(value = "codigo") String codigo ){
		System.out.println(a + " --- " + codigo);
		if (!lista_de_interesses.containsKey(a))
			lista_de_interesses.put(a,new ArrayList<String>());
		if (!lista_de_interesses.get(a).contains(codigo))
			if (cotacoes.containsKey(codigo)) {
				System.out.println("tinha o código");
				return lista_de_interesses.get(a).add(codigo);		
			}
		return false;
	} 
	//Remover cliente na lista de Interesses 
	@DeleteMapping("/removerInteresse/{a}&{codigo}")
	public boolean removerInteresse(@PathVariable(value = "a") String a,@PathVariable(value = "codigo") String codigo) {
		if (lista_de_interesses.containsKey(a))
			return lista_de_interesses.get(a).remove(codigo);	
		return false; 
	} 
	// Insere ordem de compra ou venda na lista de ordens caso não haja nenhuma outra ordem que combine com a primeira 
	@PostMapping("/criarOrdem/{q}&{v}&{t}&{c}&{a}&{d}")
	public boolean criarOrdem(@PathVariable(value = "q") int q,@PathVariable(value = "v") double v,@PathVariable(value = "t") boolean type,@PathVariable(value = "c") String c,@PathVariable(value = "a") String a,@PathVariable(value = "d") String dateString) throws ParseException {
		SimpleDateFormat form = new SimpleDateFormat("dd-MM-yyyy-hh-mm");
		Date p =  form.parse(dateString);
		lock(lock_ordem,fila_ordem,a);
		Ordem o = new Ordem(c,q,v,p,a);
		boolean reply = true;
		if (!executaTransacao(o,type)){
			String code_type = c + (type?"V":"C");
			if (!ordens.containsKey(code_type))
				ordens.put(code_type,new TreeSet<Ordem>(new Comp_Ordem()));
			reply = ordens.get(code_type).add(o);
			print(c,type);
		}
		else {
			System.out.println("****Ocorreu transação*****");
			print(c,type);
		}
		unlock(lock_ordem);
		return reply;
	}
	// Faz o match entre compra e venda e deixa todo mundo feliz 
	static public boolean executaTransacao(Ordem o1 ,boolean type ) { 
		String code_type = o1.codigo + (type?"C":"V"); // cria um código para procurar na lista correta 
		if (ordens.containsKey(code_type)){
			while ((!ordens.get(code_type).isEmpty())){
				Ordem o2 = type?ordens.get(code_type).last():ordens.get(code_type).first(); 
				double vcliente = (o1.valor)*(type?-1:1);
				double vfila = 	  (o2.valor)*(type?-1:1);
				if((o1.quantidade<1)||(vcliente<vfila))
					return o1.quantidade == 0;
				Date today = new Date();
				if (today.compareTo(o2.prazo) <= 0){
					double valor = (min (vcliente,vfila))*(type?-1:1); // ver depois << meter notificação
					int quantidade = min (o1.quantidade,o2.quantidade);
					atualizaCotacoes(o1.codigo,valor,o1.acionista); 
					pleaseNotify(o1.acionista,o1.codigo,quantidade,valor, type);
					pleaseNotify(o2.acionista,o2.codigo,quantidade,valor,!type);
					o1.quantidade -= quantidade;
					o2.quantidade -= quantidade;
					if (o2.quantidade == 0)
						ordens.get(code_type).remove(o2);
				}
				else {
					ordens.get(code_type).remove(o2);	
				} 
			}
		}
		return o1.quantidade == 0;
	} 
	// atualiza cotações depois de uma transação 
	static public void atualizaCotacoes(String c , Double v, String a){
		lock(lock_cotacoes,fila_cotacoes,a);
		Double v_antigo = cotacoes.get(c); 
		cotacoes.put(c,v);
		Double sup = max(v_antigo,v);
		Double inf = min(v_antigo,v);
		if (notificacoes.containsKey(c)){
			Iterator<Notificacao> it = notificacoes.get(c).iterator();
			while (it.hasNext()){
				Notificacao n = it.next();
				if (n.v >= inf && n.v <= sup){
					pleaseNotify(n.a, "A acao "+c+" atingiu o valor "+ Double.toString(v) + ", o programado era "+ Double.toString(n.v));
					it.remove();
				}
				else if ( n.v > sup) break;
			}
		}
		unlock(lock_cotacoes);
	}

	
	// Notifica cliente caso uma notificação programada seja atingida 
	static public void pleaseNotify(String a, String s){
		try {
		    String url = "http://localhost:"+a+"/notificarEvento1";
			URL server = new URL(url);
		    HttpURLConnection tc = (HttpURLConnection) server.openConnection();
		    tc.setRequestMethod("POST");
		    tc.setDoOutput(true);
		    tc.setRequestProperty("Content-Type", "application/json; utf-8");
		    String jsonInputString = "{\"ms\": \""+s+"\"}";
		    try(OutputStream os = (OutputStream) tc.getOutputStream()) {
		        byte[] input = jsonInputString.getBytes("utf-8");
		        os.write(input, 0, input.length);			
		    }
		    
		    
		    BufferedReader in = new BufferedReader(new InputStreamReader(tc.getInputStream()));
		    in.close();
		    
		    System.out.println("Cliente Notificado!");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	// Notifica cliente quando ocorre uma transação
	static public void pleaseNotify(String a, String c, int q ,double valor, boolean type){
		try {
		    String url = "http://localhost:"+a+"/notificarEvento2/"+c+"&"+Integer.toString(q)+"&"+Double.toString(valor)+"&"+Boolean.toString(type);
			URL server = new URL(url);
		    HttpURLConnection tc = (HttpURLConnection) server.openConnection();
		    tc.setRequestMethod("POST");
		    BufferedReader in = new BufferedReader(new InputStreamReader(tc.getInputStream()));
		    in.close();
		    System.out.println("Cliente Notificado!");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	} 
	// obtem as cotações atuais da lista de cotações conforme o cliente 
	@GetMapping("/obterCotacoes/{a}")
	public String obterCotacoes( @PathVariable(value="a") String a ) { 
		lock(lock_cotacoes,fila_cotacoes,a);
		String ans = ""; 
		if (lista_de_interesses.containsKey(a)){
			ListIterator<String>  iterator = lista_de_interesses.get(a).listIterator(); 
	        while (iterator.hasNext()) { 
	            String acao = iterator.next();
	            ans += acao + " : "+Double.toString(cotacoes.get(acao))+"\n";
	 	    }
		}
		unlock(lock_cotacoes);
		return ans;
	} 	
	// Registra CLiente Na lista de Notificações com sua notificação
	@PostMapping("/registraNotificacao/{a}&{c}&{l}")
	public void registrarNotificacao(@PathVariable(value = "a") String a, @PathVariable(value = "c") String c, @PathVariable(value = "l") double l){
		lock(lock_notificacao,fila_notificacao,a); 
		if (cotacoes.containsKey(c)){
			Notificacao n = new Notificacao(a,cotacoes.get(c) + l);
			if (!notificacoes.containsKey(c))
				notificacoes.put(c,new TreeSet<Notificacao>(new Comp_Notificacoes()));
			notificacoes.get(c).add(n);
			System.out.println("Registrada a notificacao de "+ ((l<0.0)?"Perda":"Ganho") +" da ação "+c+" para o valor de "+Double.toString(cotacoes.get(c)+l));
		}
		unlock(lock_notificacao);
	}
	// lock algum recurso
	static public void lock ( Boolean item , Queue q, Object a){
		if (item == false)
			item = true;
		else{
			q.add(a);
			while ( item == true || a != q.peek());
			lock (item,q,a);
			q.remove();
		}
	}
	// unlock algum recurso
	static public void unlock (Boolean item){
		item = false;
	}
	// printa ordens quando chamadas
	public void print(String c , boolean type){
		TreeSet<Ordem> p = ordens.get(c+(type?"V":"C"));
		if (p == null) return;
		System.out.println("--------------------------------------------------------");
		if (type)
			System.out.println("Ordens de Venda:");
		else
			System.out.println("Ordens de Compra:");

		System.out.println("--------------------------------------------------------");
		for (Ordem o : p){
			o.printOrdem();
		}
		System.out.println("--------------------------------------------------------");
	}
	static double min (double a, double b){
		return a<b?a:b;
	}
	static Double min (Double a, Double b){
		return a<b?a:b;
	}
	static double max (Double a, Double b){
		return a>b?a:b;
	}
	static int min (int a, int b){
		return a<b?a:b;
	}
}
// Classe que representa um objeto de notificação
class Notificacao {
	public String a;
	public double v;
	public Notificacao (String ac, double vc){
		this.a = ac;
		this.v = vc;
	}
	public boolean equal(Notificacao n){
		return ((this.a == n.a)&&(this.v == n.v));	 
	}
}
// Classe que é usada como comparadora de objetos de notificaçao
class Comp_Notificacoes implements Comparator<Notificacao> {
	public int compare (Notificacao n1, Notificacao n2){ // se n2 deve vir antes de n1 retorna positivo
		Notificacao first = n1;
		Notificacao second = n2;
		if (n1.equal(n2)) return 0;
		return second.v<=first.v?1:-1;
	}
}

// Classe que representa um objeto de uma ordem
class Ordem {
	public String codigo;
	public int quantidade;
	public double valor;
	public Date prazo;
	public String acionista;
	public void printOrdem( ){
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm");  
		String strDate = dateFormat.format(this.prazo);
		System.out.print(this.codigo);
		System.out.print(" ");
		System.out.print(this.quantidade);
		System.out.print(" ");
		System.out.print(this.valor);
		System.out.print(" ");
		System.out.println(strDate);		
	}
	public Ordem (String c, int q, double v, Date p, String a){
		this.codigo = c;
		this.quantidade = q;
		this.valor = v;
		this.prazo = p;
		this.acionista = a;
	}
	public boolean equal(Ordem o){
		return ((this.codigo == o.codigo)&&
			    (this.quantidade == o.quantidade)&&
				(this.valor == o.valor)&&
				(this.prazo == o.prazo)&&
				(this.acionista == o.acionista));	 
	}
}
// Classe que compara ordens para que possamos ordena-las
class Comp_Ordem implements Comparator<Ordem> {
	public int compare (Ordem o1, Ordem o2){ // se o2 deve vir antes de o1 retorna positivo
		Ordem first = o1;
		Ordem second = o2;
		if (o1.equal(o2)) return 0;
		return second.valor<=first.valor?1:-1;
	}
}


