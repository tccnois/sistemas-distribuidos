import java.util.*;
import java.text.*;
import java.rmi.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.Remote;
import java.rmi.server.UnicastRemoteObject;

public class Acionista extends UnicastRemoteObject implements AcionistaInt{
	
    static double saldo ;   // representa o tanto de dinheiro que o acionista tem
    static HomebrokerInt s; // objeto do homebroker
    static AcionistaInt c;  // objeto do acionista
    static Map<String,Acao> ativos = new HashMap<String,Acao>(); // representa a lsita de açoes que o acionista tem

	public Acionista() throws RemoteException{}

    static public void init(){
    	int MAX = 525287; // variavel usada para dar um valor aleatorio de saldo pro cliente para testes
		String code; 	  // usada para decidir quais acoes aleatorias o cliente terá
		Random r = new Random(); 
    	saldo = (double)((r.nextInt()%1000000)&MAX);  
    	for (int i = 0; i < 20; i++){
    		if ((r.nextInt()%2) == 1){
    			code = "A0"+String.format("%02d",i);
    			ativos.put(code,new Acao(code,((r.nextInt()&MAX)%100)+1,(double)(((r.nextInt()&MAX)%100+1))));
    			try{
 	   				if (s.inserirInteresse(c,code)){
 	   					System.out.println("interesse Inserido");
 	   				}
    			}
    			catch (Exception e) {
		           	System.err.println("Client exception: " + e.toString());
		            e.printStackTrace();
		        }
    		}
    	}
    }

	public void notificarEventos (String not)throws RemoteException{
        System.out.println("--------------------------------------------------------");
        System.out.println("[ALERTA PROGRAMADO]");
        System.out.println(not);
    	System.out.println("--------------------------------------------------------");
    }
    public void notificarEventos (String code, int q ,double valor, boolean type) throws RemoteException{
        System.out.println("--------------------------------------------------------");
        System.out.println("Caro cliente, A ação "+code+" foi "+(type?"Vendida ":"Comprada ")+"com sucesso.");
        System.out.println("quantidade negociada: "+Integer.toString(q));
        System.out.println("valor transacionado: "+Double.toString(valor));
        System.out.println("--------------------------------------------------------");
        saldo += valor*(type?1:-1)*q;
        if (!ativos.containsKey(code)) 
        	ativos.put(code,new Acao(code,0,0));
        Acao acao = ativos.get(code);
        if (!type){
        	s.inserirInteresse(c,code); // quando ação é comprada insere interesse na lista de interesses 
        	acao.pm= (acao.pm*acao.quantidade + valor*q)/(acao.quantidade+q);
        }
        acao.quantidade += q*(type?-1:1);

        if (acao.quantidade == 0)
            ativos.remove(acao.codigo);
        
    }
    public static boolean valid(int q, double valor){
    	return saldo >= ((double)q)*valor;
    }    
    public static boolean valid(int q, String codigo){
    	if (ativos.containsKey(codigo))
    		return ativos.get(codigo).quantidade >= q;
    	return false;
    }
    public static void mensagem(String m){
    	System.out.println("Mensagem------------------------------------------------");
    	System.out.println(m);
        System.out.println("--------------------------------------------------------");
    } 

	public static void main(String[] args)throws RemoteException{
        try {
            Registry registry = LocateRegistry.getRegistry(10099);
            s = (HomebrokerInt) registry.lookup("MarRicoHomebroker");
        	c = new Acionista();	
            Scanner inp = new Scanner(System.in);
            init();
            String codigo,datestr;
            Date d;
            SimpleDateFormat form;
            int q;
            double v,l;
           	int op = 8;
            while(op != 0){
				System.out.println("0 - Para sair");
				System.out.println("1 - Obter cotações");
				System.out.println("2 - Criar ordem de compra");
				System.out.println("3 - Criar ordem de venda");
				System.out.println("4 - registrar interesse");
				System.out.println("5 - remover interesse");
				System.out.println("6 - registrar notificacao");
				System.out.println("7 - Ver minha carteira");
				op = inp.nextInt();
                switch (op){
                	case 1:
                		System.out.println(s.obterCotacoes(c));
                		break;
                	case 2:
                		System.out.print("Código da acao: ");inp.nextLine();
                    	codigo = inp.nextLine();
                    	System.out.print("Quantidade: ");
    					q = inp.nextInt();
    					System.out.print("Preço (use vírgula): ");
    					v = inp.nextDouble();
    					System.out.print("Data de validade da ordem (dd/mm/yyyy hh:mm): ");inp.nextLine();
    	                datestr = inp.nextLine();
    	                form = new SimpleDateFormat("dd/MM/yyyy hh:mm");
    		            d = form.parse(datestr);
    		            if (valid(q,v)){
	    		            if (s.criarOrdem(codigo,q,v,d,c,false))
	       					    mensagem("Ordem de compra enviada com sucesso");
    		            	else
    		            		mensagem("Falha na operação, tente novamente");
    		            }
    		            else
    		            	mensagem("Você não tem saldo suficiente");
    					break;
    				case 3:
                		System.out.print("Código da acao: ");inp.nextLine();
                    	codigo = inp.nextLine();
                    	System.out.print("Quantidade: ");
    					q = inp.nextInt();
    					System.out.print("Preço (use vírgula): ");
    					v = inp.nextDouble();
    					System.out.print("Data de validade da ordem (dd/mm/yyyy hh:mm): ");inp.nextLine();
    	                datestr = inp.nextLine();
    	                form = new SimpleDateFormat("dd/MM/yyyy hh:mm");
    		            d = form.parse(datestr);
    		            if (valid(q,codigo)){
	    		            if (s.criarOrdem(codigo,q,v,d,c,true))
	                            mensagem("Ordem de venda enviada com sucesso");
    		            	else
    		            		mensagem("Falha na operação, tente novamente");
    		            }
    		            else
    		            	mensagem("Você não tem a quantidade de ações necesarias para efetuar a venda");

    					break;
    				case 4:
                		System.out.print("Código da acao: ");inp.nextLine();
                    	codigo = inp.nextLine();
    					if (s.inserirInteresse(c,codigo))
    						mensagem("Interesse inserido com sucesso");
    					else 
    						mensagem("Falha na operação, tenta novamente");
    					break;
    				case 5:
     					System.out.print("Código da acao: ");inp.nextLine();
                    	codigo = inp.nextLine();
    					if (s.removerInteresse(c,codigo))
    						mensagem("Interese Removido");
    					else
    						mensagem("Falha na operação, tenta novamente");
    					break;
    				case 6:
                		System.out.print("Código da acao: ");inp.nextLine();
                    	codigo = inp.nextLine();
                    	System.out.print("Limite de ganho(+) ou perda(-): ");
                    	l = inp.nextDouble();
                		s.registrarNotificacao(c, codigo, l);
                		break;
                	case 7:
                		System.out.println("Carteira------------------------------------------------");
                		System.out.println("Saldo:"+Double.toString(saldo));
                		System.out.println("Ativos:");
                		for(String acao : ativos.keySet())
         					ativos.get(acao).print();
                        System.out.println("--------------------------------------------------------");
         				break;
         			default:
         				System.out.println("Entrada INCORRETA!");
                		break;
                }
            }
                
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
	}
}
// Classe que representa uma ação
class Acao {
    String codigo;
    int quantidade;
    double pm; // preço medio de cada ação comprada, valor / quantidade;
    public Acao(String _s, int _q, double _pm){
    	this.codigo = _s;
    	this.quantidade = _q;
    	this.pm = _pm;
    }
 	public void print(){
 		System.out.println("codigo: "+codigo+"| quantidade: "+Integer.toString(quantidade)+"| preco medio: "+ String.format("%.02f",pm));
 	}
}