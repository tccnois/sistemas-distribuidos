from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps

app = Flask(__name__)

@app.route('/get')
def get():
	return jsonify('GET')

@app.route('/post')
def post():
	return jsonify('POST')

@app.route('/put')
def put():
    return jsonify('PUT')

if __name__ == '__main__':
    app.run()

"""
from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps

app = Flask(__name__)
api = Api(app)

class Users(Resource):
    def get(self):
        result = 'resultado rest GET'
        return jsonify(result)

    def post(self):
        result = 'resultado rest POST'
        return jsonify(result)

    def put(self):
        id = request.json['id']
        
        result = 'resultado rest PUT, id = '+ id 
        return jsonify(result)	"""