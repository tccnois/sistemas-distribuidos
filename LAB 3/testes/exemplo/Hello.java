package wsserver;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)

public class Hello {
    @WebMethod
    @Target(value = "LOCALHOST:1212/Hello/sayHelloRequest")
	public String sayHello(String name) {
		return "Hello " + name;
	}
	public String sayBye(String name) {
		return "Bye " + name;
	}
}