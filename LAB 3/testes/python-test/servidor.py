from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps

app = Flask(__name__)

@app.route('/oi',methods = ['POST'])
def oi():
	return 'Hello World!!!'

app.run(host = 'localhost', port = 5050)
