package wsserver;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService() // essa anotação define a classe como um endpoint do serviço web
public class CalculatorWS {
	
	@WebMethod(operationName="add") // métodos expostos aos clientes do serviço web precisam incluir a anotação @WebMethod
	
	public int add(@WebParam(name = "i") int i, @WebParam(name = "j") int j) {
		int k = i + j;
		return k;
	}
}

/*
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)

public class Hello {
    public String sayHello(String name) {
		return "Hello " + name;
	}
	public String sayBye(String name) {
		return "Bye " + name;
	}
}


package wsclient;

public class HelloClient {
	public static void main(String[] args) {
		HelloService service = new HelloService();
		Hello hello = service.getHelloPort();
		String text = hello.sayHello("hany");
		System.out.println(text);
	}
}
*/