package wsserver;

import javax.xml.ws.Endpoint;

public class ServiceStarter {
	public static void main (String[] arg){
		String url = "http://localhost:4848/Calculator";
		Endpoint.publish(url, new CalculatorWS());
		System.out.println("Service started @ " + url);
	}
}